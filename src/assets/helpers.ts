/**
 * Helper to switch css classes
 * @param classes
 * @returns boolean
 */
export function classNames(...classes: any) {
  return classes.filter(Boolean).join(" ");
}
