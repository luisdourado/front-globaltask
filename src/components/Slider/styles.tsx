import styled from "styled-components";
import background from "../../assets/images/slider/bg.jpg";

export const Container = styled.div`
  height: 250px;
  @media (max-width: 768px) {
    flex-direction: column;
    position: relative;
    width: 100%;
    flex: 1;
  }
`;

export const TextContent = styled.div`
  justify-content: flex-start;
  flex: 1;
  flex-direction: column;
  padding: 120px;
  background-image: url(${background.src});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  width: 100%;

  @media (max-width: 768px) {
    flex: 1;
    text-align: center;
    flex-direction: column;
    padding-block: 30px;
    align-items: flex-start;
  }
`;

export const Title = styled.h1`
  font-weight: bolder;
  font-size: 45px;
  line-height: 1;
  margin-bottom: 10px;

  @media (max-width: 768px) {
    flex-direction: column;
    font-size: 20px;
  }
`;

export const Paragraph = styled.p`
  font-size: 25px;
  font-weight: 500px;
  line-height: 1;
  margin-top: 10px;
  margin-bottom: 60px;

  @media (max-width: 800px) {
    flex-direction: column;
    font-size: 13px;
  }
`;

export const ImageBackground = styled.img`
  position: absolute;
  z-index: -1;
  max-height: 650px;
  width: 100%;
  align-self: center;
`;
