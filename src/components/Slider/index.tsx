import React from "react";
import {
  Container,
  TextContent,
  Title,
  Paragraph,
  ImageBackground,
} from "./styles";

import background from "../../assets/images/slider/bg.jpg";
import ButtonLink from "../ButtonLink";

export default function Slider() {
  return (
    <Container className="mb-48">
      <div className="p-30">
        <TextContent>
          <Title>Beneficiando Pessoas e Negócios</Title>
          <Paragraph>
            Transformar a vida de milhões de pessoas pelo uso <br />
            inteligente de insumos tecnológicos é nossa marca
          </Paragraph>
          <ButtonLink label="SAIBA MAIS" />
        </TextContent>
      </div>
    </Container>
  );
}
