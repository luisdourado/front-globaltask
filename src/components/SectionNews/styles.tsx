import styled from "styled-components";

export const Title = styled.h1`
  font-size: 44px;
  line-height: 50px;
  color: #333;
`;

export const Paragraph = styled.p`
  margin-top: 27px;
  font-size: 26.6px;
  color: #333;
`;

interface ICard {
  img?: string;
}

export const Card = styled.div<ICard>`
  width: 370px;
  height: 450px;
  background-image: url(${(props) => props.img});
  background-position: center;
  transition: box-shadow 0.25s;
  margin-inline: 10px;

  @media (max-width: 768px) {
    margin-top: 20px;
  }

  :hover {
    box-shadow: 0px 0px 30px 0.1px #333;
  }
`;

export const ContainerMainNews = styled.div`
  flex-direction: row;

  h2 {
    text-align: start;
  }

  @media (max-width: 800px) {
    flex-direction: column;
    justify-content: center;
    position: relative;
    align-self: center;
    text-align: center;

    img {
      margin-block: 20px;
      align-self: center;
    }

    h2 {
      margin-top: 5px;
    }

    h2,
    h3,
    p {
      text-align: center;
    }
  }
`;

export const ContainerGridNews = styled.div`
  flex: 1;
  display: flex;
  align-self: center;
  flex-direction: row;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
