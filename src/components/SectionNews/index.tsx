import React from "react";
import { Title, Card, ContainerGridNews, ContainerMainNews } from "./styles";

import ButtonLink from "../ButtonLink";

interface INews {
  category: string;
  title: string;
  img?: string;
}

const news: INews[] = [
  {
    category: "Projeto",
    title: "Piauí Conectado leva internet à 101 municípios do estado",
    img: "images/news/news-1.png",
  },
  {
    category: "Comunicações",
    title: "Tecnologia 5G trará mais avanços e modernidade ao país",
    img: "images/news/news-3.png",
  },
  {
    category: "Comunicação",
    title: "Tecnologia leva educação a distância em tempos de Covid-19.",
    img: "images/news/news-2.png",
  },
];

function GridNews() {
  return (
    <ContainerGridNews>
      {news.map((item, key) => (
        <Card
          key={key}
          className="flex flex-col items-baseline justify-end rounded-lg"
          img={item.img}
        >
          <div
            className="flex flex-col items-baseline justify-end p-5 w-screen rounded-lg"
            style={{
              width: "100%",
              height: "100%",
              backgroundImage:
                "linear-gradient(to top, black , rgba(0,0,0,0.1))",
            }}
          >
            <h1 className="text-left font-bold text-red-600">
              {item.category}
            </h1>
            <p className="text-left text-white">{item.title}</p>
          </div>
        </Card>
      ))}
    </ContainerGridNews>
  );
}

function MainNews() {
  return (
    <ContainerMainNews className="flex inline-block">
      <div className="rounded-xl flex-grow" style={{ width: "400px" }}>
        <img
          src="images/news/main-news.jpg"
          alt=""
          className="w-screen rounded-xl shadow-lg"
        />
      </div>
      <div className="flex-grow">
        <div className="flex flex-col">
          <h2 className="text-red-600 font-bold">PROJETO PIAUÍ CONECTADO</h2>
          <h3 className="text-left font-bold text-2xl">
            PPP Piauí Conectado viabiliza o trabalho remoto durante a pandemia
          </h3>
          <div className="overflow-scroll">
            <p className="text-left">
              Qui eu elit dolor ut id. Consequat minim elit eiusmod ipsum velit
              veniam aliquip do nisi. Cillum dolore elit cillum elit labore
              cupidatat. Ea aute veniam minim laborum reprehenderit dolore do
              laboris mollit incididunt dolore voluptate sit elit. Do amet Lorem
              culpa in in ullamco et veniam excepteur magna dolor velit. Anim
              nostrud aliqua anim magna veniam exercitation elit elit. Qui eu
              elit dolor ut id. Consequat minim elit eiusmod ipsum velit veniam
              aliquip do nisi. Cillum dolore elit cillum elit labore cupidatat.
              Ea aute veniam minim laborum reprehenderit dolore do laboris
              mollit incididunt dolore voluptate sit elit. Do amet Lorem culpa
              in in ullamco et veniam excepteur magna dolor velit. Anim nostrud
              aliqua anim magna veniam exercitation elit elit. Qui eu elit dolor
              ut id. Consequat minim elit eiusmod ipsum velit veniam aliquip do
              nisi. Cillum dolore elit cillum elit labore cupidatat. Ea aute
              veniam minim laborum reprehenderit dolore do laboris mollit
              incididunt dolore voluptate sit elit. Do amet Lorem culpa in in
              ullamco et veniam excepteur magna dolor velit. Anim nostrud aliqua
              anim magna veniam exercitation elit elit.
            </p>
          </div>
        </div>
      </div>
    </ContainerMainNews>
  );
}

export default function SectionNews() {
  return (
    <div className="justify-center flex flex-col text-center py-36 pb-0">
      <Title>
        Acompanhe as últimas
        <br />
        <strong>notícias sobre a Globaltask</strong>
      </Title>

      <MainNews />
      <GridNews />
      <div className="flex self-center pt-20">
        <ButtonLink label="MAIS NOTÍCIAS" />
      </div>
    </div>
  );
}
