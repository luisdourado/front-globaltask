import React from "react";
import { _links } from "./newsMock";

const ItemNews = () => {
  const [showList, setShowList] = React.useState<boolean>(true);

  React.useEffect(() => {
    window.onscroll = () => {
      document.documentElement.scrollTop > 900
      ? setShowList(false)
      : setShowList(true)
    };
  });

  return (
    <div>

      <div className="pt-3" style={{ width: "75%" }}>
        {_links.map((item) => (
          <div key={item.id}>
            <h1 className="font-bold text-red-700 mb-3" id={item.href}>
              <a href={"sobre#" + item.href}>{item.title}</a>
            </h1>
            <div className="flex items-center pb-5">
              <div className="flex-grow w-2">
                <p>{item.content}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ItemNews;
