import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { classNames } from "../../assets/helpers";

const NavbarItem = ({ name, pathname }: { name: string; pathname: string }) => {
  const [isActive, setIsActive] = useState<boolean>(false);
  const location = useRouter();

  useEffect(() => {
    if (pathname === location.asPath) {
      setIsActive(true);
    }
  }, [pathname, location.pathname]);

  return (
    <Link href={pathname}>
      <a
        className={classNames(
          isActive
            ? "mr-10 font-semibold text-gray-700 hover:text-red-600 text-base transition border-b-4 border-red-600"
            : "mr-10 font-semibold text-gray-700 hover:text-red-600 text-base transition"
        )}
      >
        {name.toUpperCase()}
      </a>
    </Link>
  );
};

export default NavbarItem;
