import styled from "styled-components";

export const Container = styled.div`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;

  @media (max-width: 768px) {
    flex-direction: column;
    align-items: center;
    margin-inline: 10px;
  }
`;

export const Nav = styled.nav`
  display: flex;

  @media (max-width: 768px) {
    align-items: center;
    justify-content: center;
    display: inline-flex;
    flex-wrap: wrap;
    margin-top: 20px;
  }
`;

export const HandleBar = styled.div`

`;