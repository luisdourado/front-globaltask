import React from "react";
import NavbarItem from "./NavbarItem";
import { Container, Nav } from "./styles";

const items = [
  { id: 0, name: "Início", pathname: "/" },
  { id: 1, name: "Sobre", pathname: "/sobre" },
];

function Navbar() {
  return (
    <Container className="flex shadow-md py-9">
      <img src="images/logo.png" alt="" style={{ width: 200 }} />
      <Nav>
        {items.map((item) => (
          <NavbarItem key={item.id} name={item.name} pathname={item.pathname} />
        ))}
      </Nav>
    </Container>
  );
}

export default Navbar;
