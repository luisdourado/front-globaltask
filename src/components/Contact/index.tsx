import React from "react";
import { Formik, Field, Form } from "formik";
import { Container, TextContainer, FormContainer, Actions } from "./styles";
import ButtonLink from "../ButtonLink";

interface IContactProps {
  name: string;
  email: string;
  phoneNumber: string;
  sector: string;
  message: string;
}

export default function Contact() {
  const initialValues: IContactProps = {
    name: "",
    email: "",
    phoneNumber: "",
    sector: "",
    message: "",
  };
  const inputStyle =
    "focus:ring-indigo-500 focus:border-indigo-600 block w-full pt-1 pb-1 pl-2 pr-12 sm:text-sm border rounded border-gray-600 bg-transparent ml-3 mb-3";
  return (
    <Container>
      <TextContainer>
        <h1>
          Fale com
          <br />a gente
        </h1>

        <div className="pt-5" style={{ width: "21rem", fontSize: 20 }}>
          <p className="text-left">
            Estamos de prontidão para atendê-lo e esclarecer dúvidas, ouvir
            suas sugestões e agendar uma visita.
          </p>
        </div>
      </TextContainer>
      <Formik
        initialValues={initialValues}
        onSubmit={(values: IContactProps) => {
          console.log(values);
        }}
      >
        <Form>
          <FormContainer>
            <div className="flex flex-row">
              <Field
                type="text"
                name="name"
                id="name"
                className={inputStyle}
                placeholder={"Nome".toUpperCase()}
              />
              <Field
                type="text"
                name="email"
                id="email"
                className={inputStyle}
                placeholder={"E-mail".toUpperCase()}
              />
            </div>
            <div className="flex flex-row">
              <Field
                type="number"
                name="phoneNumber"
                id="phoneNumber"
                className={inputStyle}
                placeholder={"Fone".toUpperCase()}
              />
              <Field
                type="text"
                name="sector"
                id="sector"
                className={inputStyle}
                placeholder={"Setor".toUpperCase()}
              />
            </div>
            <Field
              type="text"
              name="message"
              id="message"
              className="pt-1 pb-1 pl-2 pr-12 sm:text-sm border rounded border-gray-600 bg-transparent ml-3 mb-3 flex"
              style={{ justifyContent: "flex-start", height: 100, flex: 0}}
              placeholder={"Mensagem".toUpperCase()}
            />
            <Actions>
              <ButtonLink label="Enviar" />
            </Actions>
          </FormContainer>
        </Form>
      </Formik>

    </Container>
  );
}
