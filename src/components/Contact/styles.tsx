import styled from "styled-components";

export const Input = styled.input`
  border-color: #333;
  border-width: 1px;
`;

export const Container = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  text-align: center;
  align-items: center;
  padding-block: 80px;
  padding-inline: 120px;
  flex-direction: row;
  margin-top: 12px;
  background-color: #E7EBEF;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const TextContainer = styled.div`
  flex: 1;
  flex-direction: column;
  display: flex;
  align-items: flex-start;
  h1 {
    text-align: left;
    font-weight: bold;
    font-size: 60px;
    line-height: 4.4rem;
  }

  @media (max-width: 768px) {
    justify-content: center;
    text-align: center;
    align-items: center;
    
    h1 {
    text-align: center;
    font-size: 40px;
    font-weight: bold;
    line-height: 2rem;
  }
    p {
      margin-block: 15px;
    text-align: center;
    font-size: 15px;
  }
  }
`;

export const FormContainer = styled.div`
  flex: 1;
  width: '45rem';
  height: '20rem';
  background-color: #FFFF;
  padding: 15px;
  align-items: flex-start;
  display: flex;
  flex-direction: column;

  @media (max-width: 768px) {
    width: auto;
    height: auto;
    align-self: center;
    padding: 30px;
    h1 {
      text-align: center;
    }
  }
`;

export const Actions = styled.div`
  align-self: flex-end;
  @media (max-width: 768px) {
    margin-block: 10px;
    align-self: center;
  }
`;