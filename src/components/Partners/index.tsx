import React from "react";
import {
  Container,
  Title,
  Paragraph,
  ParagraphContainer,
  GridWrapper,
} from "./styles";

const partners = [
  { name: "Cisco", logo: "images/partners/cisco.png" },
  { name: "Furukawa", logo: "images/partners/furukawa.png" },
  { name: "Ciena", logo: "images/partners/ciena.png" },
  { name: "ZTE", logo: "images/partners/zte.png" },
];

export default function Partners() {
  return (
    <Container>
      <Title>
        Parceiros da <strong>Globaltask</strong>
      </Title>
      <ParagraphContainer>
        <Paragraph>
          Contamos com parcerias dos maiores players do mercado de insumos
          tecnológicos, o que nos possibilita oferecer os melhores equipamentos
          e serviços com a garantia e confiança que seu projeto merece.
        </Paragraph>
      </ParagraphContainer>

      <GridWrapper>
        {partners.map((item, key) => (
          <div key={key}>
            <img
              src={item.logo}
              alt={item.name}
              style={{ width: 125, margin: 15 }}
            />
          </div>
        ))}
      </GridWrapper>
    </Container>
  );
}
