import styled from "styled-components";

export const Container = styled.div`
  justify-content: center;
  flex: 1;
  flex-direction: column;
  text-align: center;
  justify-content: center;
  align-items: center;
  padding-top: 7rem;
`;

export const Title = styled.h1`
  font-size: 44px;
  color: #333;
`;

export const ParagraphContainer = styled.div`
  margin-inline: 60px;
`;

export const Paragraph = styled.p`
  margin-top: 10px;
  font-size: 22px;
  text-align: center;
  color: #333;

  @media (max-width: 768px) {
    font-size: 16px;
  }
`;

export const GridWrapper = styled.div`
  flex: 1;
  justify-content: center;
  align-items: center;
  display: flex;
  align-self: center;
  flex-direction: row;
  margin-block: 20px;
  @media (max-width: 768px) {
    margin-inline: 15px;
  }
`;

// "grid gap-28 grid-cols-4 items-center self-center py-16"
