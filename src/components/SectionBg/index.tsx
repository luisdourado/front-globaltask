import React from "react";
import { Title, ContainerSlideSec } from "./styles";

import SliderItem from "../SliderItem";
import ButtonLink from "../ButtonLink";

import background from "../../assets/images/section-2/cases.png";

const sliderItems = [
  {
    id: 0,
    title: "Solução educacional",
    img: "images/section/miniatura01.png",
    content:
      "Qui non culpa laborum nisi cupidatat. Aute culpa culpa cupidatat magna reprehenderit deserunt velit voluptate. Magna laborum laboris enim sint",
  },
  {
    id: 1,
    title: "Wi-fi para Acesso Público",
    img: "images/section/miniatura02.png",
    content:
      "Qui non culpa laborum nisi cupidatat. Aute culpa culpa cupidatat magna reprehenderit deserunt velit voluptate. Magna laborum laboris enim sint.",
  },
  {
    id: 2,
    title: "Soluções de Data Center",
    img: "images/section/miniatura03.png",
    content:
      "Qui non culpa laborum nisi cupidatat. Aute culpa culpa cupidatat magna reprehenderit deserunt velit voluptate. Magna laborum laboris enim sint.",
  },
  {
    id: 3,
    title: "Solução educacional",
    img: "images/section/miniatura04.png",
    content:
      "Qui non culpa laborum nisi cupidatat. Aute culpa culpa cupidatat magna reprehenderit deserunt velit voluptate. Magna laborum laboris enim sint.",
  },
];

function SliderSection() {
  return (
    <ContainerSlideSec className="flex mx-auto my-auto items-center justify-center">
      {sliderItems.map((item) => (
        <SliderItem
          key={item.id}
          img={item.img}
          title={item.title}
          content={item.content}
        />
      ))}
    </ContainerSlideSec>
  );
}

export default function Section() {
  return (
    <div
      className="justify-center flex flex-col text-center my-auto mx-auto py-20 pb-24"
      style={{
        backgroundImage: `url(${background.src})`,
        backgroundPosition: "center",
      }}
    >
      <Title>
        Cases de <strong>Sucesso</strong>
      </Title>
      <SliderSection />
      <div className="flex self-center">
        <ButtonLink label="SAIBA MAIS" />
      </div>
    </div>
  );
}
