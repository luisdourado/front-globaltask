import styled from "styled-components";

export const Title = styled.h1`
  font-size: 44px;
  color: #333;
`;

export const Paragraph = styled.p`
  margin-top: 27px;
  font-size: 26.6px;
  color: #333;
`;

export const ContainerSlideSec = styled.div`
  @media (max-width: 800px) {
    flex-direction: column;
    position: relative;
    align-self: center;
  }
`;
