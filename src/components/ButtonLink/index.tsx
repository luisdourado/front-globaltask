import React from "react";
import styled from "styled-components";

const Button = styled.button`
  @media (max-width: 768px) {
    flex: 1;
    text-align: center;
    flex-direction: column;
    font-size: 10px;
    align-items: flex-start;
  }
`;

export default function ButtonLink(props: {
  label: string;
  bgColor?: string;
  href?: string;
}) {
  return (
    <Button className="bg-red-600 hover:bg-red-700 transition text-white py-3 px-10">
      {props.label}
    </Button>
  );
}
