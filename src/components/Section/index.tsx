import React from "react";
import { Container, Title, Paragraph, ContainerSlideSec } from "./styles";

import SliderItem from "../SliderItem";

const sliderItems = [
  {
    id: 0,
    title: "Solução educacional",
    img: "images/section/miniatura01.png",
    content:
      "Qui non culpa laborum nisi cupidatat. Aute culpa culpa cupidatat magna reprehenderit deserunt velit voluptate. Magna laborum laboris enim sint",
  },
  {
    id: 1,
    title: "Wi-fi para Acesso Público",
    img: "images/section/miniatura02.png",
    content:
      "Qui non culpa laborum nisi cupidatat. Aute culpa culpa cupidatat magna reprehenderit deserunt velit voluptate. Magna laborum laboris enim sint.",
  },
  {
    id: 2,
    title: "Soluções de Data Center",
    img: "images/section/miniatura03.png",
    content:
      "Qui non culpa laborum nisi cupidatat. Aute culpa culpa cupidatat magna reprehenderit deserunt velit voluptate. Magna laborum laboris enim sint.",
  },
  {
    id: 3,
    title: "Solução educacional",
    img: "images/section/miniatura04.png",
    content:
      "Qui non culpa laborum nisi cupidatat. Aute culpa culpa cupidatat magna reprehenderit deserunt velit voluptate. Magna laborum laboris enim sint.",
  },
];

function SliderSection() {
  return (
    <ContainerSlideSec className="flex flex-row justify-center items-center mx-auto my-auto">
      {sliderItems.map((item) => (
        <SliderItem
          key={item.id}
          img={item.img}
          title={item.title}
          content={item.content}
        />
      ))}
    </ContainerSlideSec>
  );
}

export default function Section() {
  return (
    <Container className="justify-center flex flex-col text-center">
      <Title>
        Conheça o <strong>Globaltask</strong>
      </Title>
      <Paragraph>
        A Globaltask Tecnologia e Gestão é uma empresa voltada para o <br />
        desenvolvimento, implementação e suporte profissional de soluções
        <br /> inteligentes de comunicação de dados, voz e imagem.
      </Paragraph>

      <SliderSection />
    </Container>
  );
}
