import styled from "styled-components";

export const Container = styled.div`
  padding-block: 5rem;
  @media (max-width: 768px) {
    padding-block: 1rem;
    align-items: center;
    justify-content: center;
    justify-items: center;
  }
`;

export const Title = styled.h1`
  font-size: 44px;
  color: #333;

  @media (max-width: 768px) {
    font-size: 30px;
  }
`;

export const Paragraph = styled.p`
  margin-top: 27px;
  font-size: 26.6px;
  color: #333;

  @media (max-width: 768px) {
    font-size: 11.7px;
    text-align: center;
  }
`;

export const ContainerSlideSec = styled.div`
  @media (max-width: 800px) {
    flex-direction: column;
    position: relative;
    align-self: center;
  }
`;
