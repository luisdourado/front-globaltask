import styled from "styled-components";

export const Container = styled.div`
  padding-block: 100px;
  padding-inline: 100px;

  @media (max-width: 768px) {
    flex-wrap: wrap;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    justify-items: center;
    padding-block: 30px;
    h1,
    p {
      font-size: 10px;
      text-align: center;
    }
  }
`;

export const Box = styled.div`
  flex-grow: 1;
  margin-bottom: 10px;
  margin-inline: 20px;
`;

export const Copywrite = styled.p`
  text-align: right;
  margin-top: 10px;
  font-size: 11px;
  @media (max-width: 768px) {
    text-align: center;
  }
`;
