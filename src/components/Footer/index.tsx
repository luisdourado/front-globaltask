import React from "react";
import { Container, Box, Copywrite } from "./styles";

export default function Footer() {
  return (
    <footer>
      <Container className="flex justify-center items-center bg-white">
        <Box>
          <h1 className="font-bold">Cuiabá - MT</h1>
          <p>
            Avenida José Monteiro de Figueiredo, 1826-A, Sala 3, Duque de Caxias
            I, Cuiabá/MT CEP: 78043-300.
          </p>
        </Box>
        <Box>
          <h1 className="font-bold">SAC</h1>
          <p>
            Capitais e Regiões Metropolitanas: 3055-7700 Outras Regiões: (DDD
            Local) + 3055-7700 Email: comercial@globaltask.com.br
          </p>
        </Box>
        <div className="items-end justify-end flex-grow">
          <img src="images/logo.png" alt="" style={{ width: 300 }} />
          <Copywrite>
            Todos os direitos reservados à <strong>Globaltask</strong>
          </Copywrite>
        </div>
      </Container>
    </footer>
  );
}
