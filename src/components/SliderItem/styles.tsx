import styled from "styled-components";

interface ICard {
  width: number;
  height?: number;
}

const HEIGHT = 330;

export const Card = styled.div<ICard>`
  width: ${(props) => props.width}px;
  height: ${HEIGHT}px;
  transition: all 0.25s;
  transition-delay: 0s;
  box-shadow: 0 10px 25px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);

  :hover {
    transition-delay: 0.35s;
    height: ${HEIGHT + 40}px;
    width: ${(props) => props.width + 40}px;
    box-shadow: 0 10px 25px -3px rgba(211, 0, 0, 0.678),
      0 4px 6px -2px rgba(0, 0, 0, 0.05);
  }

  margin-block: 30px;
  @media (max-width: 768px) {
  }
`;
