import React from "react";
import { Card } from "./styles";

export default function SliderItem(props: {
  title: string;
  img?: string;
  content: string;
}) {
  return (
    <Card
      width={240}
      className="rounded-xl bg-white mx-2 items-center justify-center flex flex-col"
    >
      <div style={{ overflow: "hidden", flex: 1, height: 200 }}>
        <img
          src={props.img ?? "https://via.placeholder.com/240x130"}
          alt=""
          style={{
            flex: 1,
            borderStartEndRadius: "0.5rem",
            borderStartStartRadius: "0.5rem",
          }}
        />
      </div>
      <div className="p-5 flex flex-col">
        <h1 className="h1 font-bold mb-2" style={{ fontSize: 20 }}>
          {props.title}
        </h1>
        <p style={{ fontSize: 13 }}>{props.content}</p>
      </div>
    </Card>
  );
}
