import type { NextPage } from "next";
import React from "react";
import styled from "styled-components";

import background from "../src/assets/images/slider/bg.jpg";

import Navbar from "../src/components/Navbar";
import ItemNews from "../src/components/ItemNews";
import Contact from "../src/components/Contact";
import Footer from "../src/components/Footer";

const Sobre: NextPage = () => {
  return (
    <PageContainer className="absolute w-full">
      <Navbar />
      {/* Início - Imagem */}
      <Image />
      {/* Fim - Imagem */}

      {/* Conteudo */}
      <Content>
        <div className="flex flex-row items-center">
          <p className="font-semibold text-xs">Sobre a GlobalTask</p>
          <p className="font-bold text-xs text-red-700 ml-1">
            {">"} Globaltask
          </p>
        </div>

        <ItemNews />
      </Content>
      <Contact />
      <Footer />
    </PageContainer>
  );
};

const PageContainer = styled.div`
  flex: 1;

  @media (max-width: 768px) {
    flex-direction: row;
    position: absolute;
    width: 100%;
  }
`;

const Content = styled.div`
  padding: 100px;
  padding-top: 30px;

  @media (max-width: 768px) {
    padding: 10px;
    padding-top: 20px;
    flex: 1;
    display: flex;
    justify-content: center;
    flex-direction: column;
    display: flex;
  }
`;

export const Image = styled.div`
  justify-content: flex-start;
  flex: 1;
  flex-direction: column;
  padding: 120px;
  background-image: url(${background.src});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  width: 100%;

  @media (max-width: 768px) {
    flex: 1;
    text-align: center;
    flex-direction: column;
    padding-block: 30px;
    align-items: flex-start;
  }
`;

export default Sobre;
