import type { NextPage } from "next";
import React from "react";
import styled from "styled-components";

import Navbar from "../src/components/Navbar";
import Slider from "../src/components/Slider";
import Section from "../src/components/Section";
import SectionBg from "../src/components/SectionBg";
import SectionNews from "../src/components/SectionNews";
import Partners from "../src/components/Partners";
import Contact from "../src/components/Contact";
import Footer from "../src/components/Footer";

const Home: NextPage = () => {
  return (
    <PageContainer className="absolute w-full">
      <Navbar />
      <Slider />
      <Section />
      <SectionBg />
      <SectionNews />
      <Partners />
      <Contact />
      <Footer />
    </PageContainer>
  );
};

const PageContainer = styled.div`
  flex: 1;

  @media (max-width: 768px) {
    flex-direction: row;
    position: absolute;
    width: 100%;
  }
`;

export default Home;
